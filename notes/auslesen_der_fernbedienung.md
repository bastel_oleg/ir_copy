# Auslesen der Fernbedienung

<img src="./bilder/fernbedienung.jpg">

# Schaltung zum Sacnnen

<img src="./bilder/ir_scanner_schaltung.png">

# Steckbrett

<img src="./bilder/steckbrett.jpg">

# Scann "Auto" Knopf

<img src="./bilder/scann_autobtn.png">

# Scann "3H" Knopf

<img src="./bilder/scann_3hbtn.png">

# Sann "5H" Knopf

<img src="./bilder/scann_5hbtn.png">

# Scann "8H" Knopf

<img src="./bilder/scann_8hbtn.png">

# Scann "Heller" Knopf

<img src="./bilder/scann_hellerbtn.png">

# Scann "Dunkler" Knopf

<img src="./bilder/scann_dunklerbtn.png">

# Scann "ON" Knopf

<img src="./bilder/scann_onbtn.png">

# Scann "OFF" Knopf

<img src="./bilder/scann_offbtn.png">

# Genaue Signalzeiten

<img src="./bilder/scann_signalzeiten1.png">

<img src="./bilder/scann_signalzeiten2.png">

# Resultat
Aus den vorligenden Signalen kann man folgendes Muster erkennen.

$$ R =  \dfrac{5-1}{0.02} $$


```python

```
